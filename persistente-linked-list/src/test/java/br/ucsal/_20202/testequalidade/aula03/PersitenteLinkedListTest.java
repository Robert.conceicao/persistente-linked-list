package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

public class PersitenteLinkedListTest {

	private static final String TAB_NAME = "lista1";

	private static final Long ID_LISTA = 100L;

	private static PersistentList<String> nomes;

	@BeforeEach
	public void setup() throws SQLException {
		assumeTrue(DbUtil.isConnectionValid());
		nomes = new PersitenteLinkedList<>();
		limparLista();
	}

	@AfterAll
	public static void teardown() throws SQLException {
		assumeTrue(DbUtil.isConnectionValid());
		limparLista();
	}

	@Test
	@DisplayName("Teste de persitência de uma lista com 4 elementos")
	public void testPersist4Elements()
			throws InvalidElementException, SQLException, IOException, ClassNotFoundException {
		// Dados de entrada
		// Execução do método a ser testado
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("pedreira");
		nomes.add("neiva");

		nomes.persist(ID_LISTA, DbUtil.getConnection(), TAB_NAME);

		nomes.clear();

		nomes.load(ID_LISTA, DbUtil.getConnection(), TAB_NAME);

		// Saída esperada
		// Verificação se a saída esperada é igual à saída atual
		assertAll("Verificar add", () -> assertEquals(4, nomes.size()),
				() -> assertEquals("antonio", nomes.get(0), "O nome na posição 0 deveria ser antonio"),
				() -> assertEquals("claudio", nomes.get(1), "O nome na posição 1 deveria ser claudio"),
				() -> assertEquals("pedreira", nomes.get(2)), () -> assertEquals("neiva", nomes.get(3)));
	}

	private static void limparLista() throws SQLException {
		nomes.delete(ID_LISTA, DbUtil.getConnection(), TAB_NAME);
	}

}
