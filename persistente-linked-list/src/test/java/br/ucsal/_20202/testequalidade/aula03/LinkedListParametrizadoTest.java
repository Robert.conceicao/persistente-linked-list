package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

public class LinkedListParametrizadoTest {

	private List<String> nomes;

	@BeforeEach
	public void setup() throws InvalidElementException {
		nomes = new LinkedList<>();
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("pedreira");
		nomes.add("neiva");
	}

	@ParameterizedTest(name = "{index} get({0})= {1}")
	@CsvSource({ "0,antonio", "1,claudio", "2,pedreira", "3,neiva" })
	public void testGet(Integer i, String nomeEsperado) throws InvalidElementException {
		assertEquals(nomeEsperado, nomes.get(i));
	}

	@Test
	public void testGetPosicaoInvalida() {
		assertNull(nomes.get(10));
	}

	@Test
	public void testClear() {
		int qtdEsperada = 0;
		nomes.clear();
		int qtdAtual = nomes.size();
		assertEquals(qtdEsperada, qtdAtual);
	}

}
